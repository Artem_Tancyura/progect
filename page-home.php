<?php get_header(); ?>


<!--First flexslider-->
<div class="site-content clearfix">

<?php if ( function_exists('show_nivo_slider') ) { show_nivo_slider(); } ?>

    <div class="container clearfix">

<!--About Us-->
        <div class="about-us">
            <h2 class="about-us-caption">
               &#1055;&#1056;&#1048;&#1063;&#1048;&#1053;&#1048; </br> &#1050;&#1059;&#1055;&#1059;&#1042;&#1040;&#1058;&#1048; &#1044;&#1054;&#1041;&#1056;&#1048;&#1042;&#1040; &#1059; &#1053;&#1040;&#1057;
               <!--Причини <span>замовити обладнання у нас</span>-->
            </h2>


           <?php $query = new WP_Query(
            array(
            'category_name' => 'about-us',
            'posts_per_page' =>'-1')
            );?>

            <div class="about-us">
                <ul class="about-us-list clearfix">
                    <?php if ( $query->have_posts() ) {
                        while ( $query->have_posts() ) {
                            $query->the_post();
                            ?>
                            <li class="col-xs-6 col-sm-15 col-md-15 col-lg-15">
                                <div class="thumbnail-about-us"> <img src= " <?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?> "/>  </div>
                                <h3 class="about-us-title"><?php the_title(); ?></h3>
                                <p class="about-us-content"> <?php echo get_the_content();?> </p>
                            </li>
                        <?php } } ?>

                </ul>
            </div>
        </div>
<!--About Us-->

<!--About Us-->
    <div class="our_services">
        <div class="container ">
            <h2 class="main-title-department"> &#1042;&#1048;&#1053;&#1048;&#1050;&#1051;&#1048; &#1055;&#1048;&#1058;&#1040;&#1053;&#1053;&#1071;? </br> &#1047;&#1042;&#1045;&#1056;&#1058;&#1040;&#1049;&#1058;&#1045;&#1057;&#1071;! </h2>

            <div class="services_list col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <img src="<?php bloginfo('template_directory'); ?>/images/images/fajl.png" alt="img" class="thumbnail-our-department">
                <h2> &#1041;&#1091;&#1093;&#1075;&#1072;&#1083;&#1090;&#1077;&#1088;&#1110;&#1103; </h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque</br>
                    elementum augue sed est portitir, ac blandit nisi pousuere.  </p>
            </div>
            <div class="services_list col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <img class="thumbnail-our-department" alt="2" src="<?php echo get_template_directory_uri(); ?>/images//images/potok_rynka_diagramma.png" />
                <h2> &#1070;&#1088;&#1080;&#1076;&#1080;&#1095;&#1085;&#1080;&#1081; &#1074;&#1110;&#1076;&#1076;&#1110;&#1083; </h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque</br>
                    elementum augue sed est portitir, ac blandit nisi pousuere.  </p>
            </div>
            <div class="services_list col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <img class="thumbnail-our-department" alt="3" src="<?php echo get_template_directory_uri(); ?>/images//images/mirovaja_statistika.png" />
                <h2> &#1040;&#1075;&#1088;&#1086;&#1085;&#1086;&#1084;&#1110;&#1095;&#1085;&#1080;&#1081; &#1074;&#1110;&#1076;&#1076;&#1110;&#1083; </h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque</br>
                    elementum augue sed est portitir, ac blandit nisi pousuere.  </p>
            </div>
            <div class="services_list col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <img class="thumbnail-our-department" alt="4" src="<?php echo get_template_directory_uri(); ?>/images//images/fajl.png" />
                <h2> &#1051;&#1086;&#1075;&#1110;&#1089;&#1090;&#1080;&#1095;&#1085;&#1080;&#1081; &#1074;&#1110;&#1076;&#1076;&#1110;&#1083; </h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque</br>
                    elementum augue sed est portitir, ac blandit nisi pousuere.  </p>
            </div>

        </div>
    </div>
        <!--About Us-->

<!--Second flexslider-->
        <?php $query = new WP_Query(
            array(
                'post_type' => 'product'
//                'posts_per_page' =>
            )
        );
        ?>

        <h2 class="flexslider2-title"><a href="<?php echo  esc_url(get_permalink(get_page_by_path('products'))); ?>"> <?php echo (get_the_title(get_page_by_path('products'))); ?> </a></h2>
        <div class="flexslider clearfix" id="flexslider2">
            <ul class="slides clearfix">

                <?php  if ( $query->have_posts() ) {?>



                   <?php while ( $query->have_posts() ) {
                        $query->the_post();
                        ?>
                        <?php if ( has_post_thumbnail() ) { ?>
                            <li class="flex2-item">
                                <a class="thumbnail-container" href="<?php the_permalink(); ?>"> <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>" /></a>
                                <p class="flex-caption"> <?php the_title(); ?></p>
                            </li>
                        <?php } ?>

                    <?php }  }
                         else {  ?>

                    <p>
                        <?php __('No content found' , 'theme_text_domain'); ?>
                    </p>

                <?php } ?>
            </ul>
    </div>
<!--Second flexslider-->

<!--Video in page        -->
        <div class="home-page-video">


       <!--        --><?php //echo do_shortcode('[video mp4="video/1.mp4" ogv="source.ogv" webm="source.webm"]'); ?>

            <iframe width="540" height="315" src="https://www.youtube.com/embed/xJXXjRJYuuI" frameborder="0" allowfullscreen></iframe>

       </div>
            <!--Video in page  -->

        <!-- social news -->

        <div class="home-news-sokial">
            <a class="twitter-timeline" href="https://twitter.com/apis_holding" data-widget-id="722553928555081729">&#1058;&#1074;&#1080;&#1090;&#1099; &#1086;&#1090; @apis_holding</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
        </div>
</div>
<!--site content-->
<?php get_footer();?>