<?php get_header(); ?>

    <!-- site content -->
    <div class="site-content container">
        <!--main-column-->
        <div class="main-column">

           <div class="page-contact-form col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
            <?php if (is_active_sidebar('contact-form2')) :?>
                <div class="form-widget-area clearfix">
                    <?php dynamic_sidebar('contact-form2'); ?>
                </div>
        </div>
            <?php endif;?>
        </div>
        <!--main-column-->

        <div class="sidebar">
            <?php get_sidebar(); ?>
        </div>

    </div>
    <!--site content-->


<?php get_footer(); ?>