<?php get_header(); ?>



    <!-- site content -->
    <div class="site-content container">
    <!--main-column-->
    <div class="main-column">

        <div class="our-info">

    <h2 class="about-us-caption">
        про нас <span>дізнайтеся про нашу діяльність більше</span>
    </h2>


    <?php $query = new WP_Query(
        array(
            'category_name' => 'our-info',
            'posts_per_page' =>'3')
    );?>

    <div class="our-info">
        <ul class=" clearfix">
            <?php if ( $query->have_posts() ) {
                while ( $query->have_posts() ) {
                    $query->the_post();
                    ?>
                    <li class="about-us-list">
                        <h3 class="our-info-title"><?php the_title(); ?></h3>
                        <div class="thumbnail-our-info">  <?php the_post_thumbnail(); ?>  </div>
                        <p class="our-info-content"> <?php echo the_content('дізнатися більше');?> </p>
                    </li>
                <?php } } ?>

        </ul>
    </div>
</div>

    </div>

    <div class="sidebar">
        <?php get_sidebar(); ?>
    </div>

</div>
    <!--site content-->

<?php get_footer();?>