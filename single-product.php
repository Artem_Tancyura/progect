<?php get_header(); ?>

    <!-- site content -->
    <div class="site-content container ">
    <!--main-column-->
    <div class="main-column ">

        <div class="products-posts clearfix">
                <?php


                if ( have_posts() ) {
                    while ( have_posts() ) {
                       the_post();
                        ?>

                            <h1 class="product-single-title">  <?php the_title();?></h1>

                        <div class="left-single-post col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div class="single-thumbnail ">
                                <?php if ( has_post_thumbnail() ) { ?>
                                     <?php the_post_thumbnail('full');?>
                                <?php } else {?> <a href="<?php the_permalink(); ?>"> <img src="<?php echo get_template_directory_uri(); ?>/images/default_image.png" alt="No thumbnail"" alt=""> </a>

                                <?php } ?>

                            </div>
                        </div>

                            <?php if (get_post_meta($post->ID, 'old_price', true)) { ?>
                                <p class="old-price-single">
                                    &#1062;&#1110;&#1085;&#1072;: <?php echo get_post_meta($post->ID, 'old_price', true);?> &#1075;&#1088;&#1085;.
                                </p>
                            <?php }
                            else {
                                ?>
                                <a class="no-price-single" href="<?php echo  esc_url(get_permalink(get_page_by_path('contact')));?> ">
                                    <?php echo __(' &#1047;&#1074;\'&#1103;&#1078;&#1110;&#1090;&#1100;&#1089;&#1103; &#1079; &#1085;&#1072;&#1084;&#1080; &#1076;&#1083;&#1103; &#1091;&#1090;&#1086;&#1095;&#1085;&#1077;&#1085;&#1085;&#1103; &#1094;&#1110;&#1085;&#1080;','index.php'); ?>
                                </a>
                            <?php } ?>



                        <div class="right-single-post col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                      <?php echo the_content(); ?>
                        </div>


                        <?php
                    }
                } else {
                    // &#1055;&#1086;&#1089;&#1090;&#1086;&#1074; &#1085;&#1077; &#1085;&#1072;&#1081;&#1076;&#1077;&#1085;&#1086;
                }
                ?>
        </div>
        <!--main-column-->

    </div>

        <div class="sidebar">
           <?php get_sidebar(); ?>
        </div>


    </div>
    <!--site content-->
<?php get_footer();?>